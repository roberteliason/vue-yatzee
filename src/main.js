import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

function createCirclePattern(width = 100, height = 100, circles = 5) {
  let x = (width / 4) * -1;
  let ySep = height / (circles);
  let shapes = '';
  for (let r = 0; r <= 1; r++) {
    x += width/2;
    for (let i = 1; i <= circles; i++) {
      let y = ySep * i - (ySep / 2);
      let cr = Math.abs(Math.sin(i)*width/4);
      if (r%2 === 0) {
        y = height - y;
      }
      shapes += '<ellipse cx="'+x+'" cy="'+y+'" rx="'+cr+'" ry="'+cr+'" fill="rgba(0,0,0,0.05)"/>';
    }
  }

  return shapes;
}

function createSVG(width = 100, height = 100, shapes = '<ellipse cx="50" cy="50" rx="40" ry="40" />') {
  return "<svg xmlns='http://www.w3.org/2000/svg' width='"+width+"' height='"+height+"'>"+shapes+"</svg>";
}

function encodeSVG(SVG = '') {
  return window.btoa(SVG);
}

function paintSVG(encodedSVG) {
  window.document.getElementsByTagName('body')[0].style.backgroundImage = "url('data:image/svg+xml;base64," + encodedSVG + "')";
}

paintSVG(encodeSVG(createSVG(40, 120, createCirclePattern(40, 120, 6))));